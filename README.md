# IDS721_week6 Mini Project (Extending from week 5 project)

## Step

### Create the table in AWS DynamoDB
Add attributes [Artist] and [SongTitle]
Add three items:
[Artist1]: [Song1]
[Artist2]: [Song2]
[Artist3]: [Song3]

### Update the src/main.rs

### cargo lambda build --release
### cargo lambda deploy

### enable X-Ray active tracing

### Result
![image](image_1.png)
![image](image_2.png)
![image](image_3.png)
![image](image_4.png)

