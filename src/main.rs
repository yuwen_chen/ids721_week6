// Time: 03/01/2024
// Project: IDS721 Week 5
// Import libraries

use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};

use std::collections::HashMap;
use serde_json::{json, Value};
use serde::{Deserialize, Serialize};
use rand::prelude::*;
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};

use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;

// Function to request song name
#[derive(Serialize)]
struct Response {
    title_of_song: String,
}

// Write the function to request the artist name
#[derive(Deserialize)]
struct Request {
    artist: Option<String>,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();

    info!("Service starting");
    tracing::subscriber::set_global_default(subscriber).expect("Error");

    let main_func = service_fn(handler);
    lambda_runtime::run(main_func).await?;
    Ok(())
}

// Function to search the song
async fn main_function_to_search_song(client_name: &Client, artist_name: Option<String>) -> Result<String, LambdaError> {
    let table_var = "week5_music";
    let mut exp_values = HashMap::new();
    let mut expr = String::new();
    if let Some(artist_var) = artist_name {
        exp_values.insert(":input_artist".to_string(), AttributeValue::S(artist_var));
        expr.push_str("Artist = :input_artist");
    }
    let search_result = client_name.scan()
        .table_name(table_var)
        .set_expression_attribute_values(Some(exp_values))
        .set_filter_expression(Some(expr))
        .send()
        .await?;
    let search_items = search_result.items.unwrap_or_default();
    let chosen_item = search_items.iter().choose(&mut thread_rng());
    // Find the song based on the artists name
    match chosen_item {
        Some(var) => {
            let title_var = var.get("SongTitle").and_then(|val| val.as_s().ok());
            match title_var {
                Some(title_of_song) => Ok(title_of_song.to_string()),
                None => Err(LambdaError::from("Song not found!")),
            }
        }, None => Err(LambdaError::from("Error!")),
    }
}

async fn handler(event_name: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request_holder: Request = serde_json::from_value(event_name.payload)?;
    let var_config = load_from_env().await;
    let client_name = Client::new(&var_config);
    let title_of_song = main_function_to_search_song(&client_name, request_holder.artist).await?;
    Ok(json!({
        "SongTitle": title_of_song,
    }))
}
